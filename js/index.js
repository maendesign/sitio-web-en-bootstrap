$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
              
$(function () {
    $('[data-toggle="popover"]').popover()
});

$('.carousel').carousel({
interval: 4000
});

$("#solicitarInformacion").on("show.bs.modal", function(e){

console.log("La ventana modal está apareciendo");

});

$("#solicitarInformacion").on("shown.bs.modal", function(e){

console.log("La ventana modal ha aparecido");
$(".botonAbreModal").removeClass("btn-primary");
$(".botonAbreModal").addClass("btn-danger");
$(".botonAbreModal").prop('disabled', true);

});

$("#solicitarInformacion").on("hide.bs.modal", function(e){

console.log("La ventana modal se está ocultando");

});

$("#solicitarInformacion").on("hidden.bs.modal", function(e){

console.log("La ventana modal se ha ocultado");
$(".botonAbreModal").removeClass("btn-danger");
$(".botonAbreModal").addClass("btn-primary");
$(".botonAbreModal").prop('disabled', false);
});